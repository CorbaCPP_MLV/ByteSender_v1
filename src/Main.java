import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.omg.CORBA.ORB;

import bytes_transmission.Echo;
import bytes_transmission.EchoHelper;

public class Main {

	private static final String imagePath = "/home/davide/flowers.png";
	private static String SERVER_IOR_FNAME = "/tmp/ior";
	private static Echo ECHO;

	public static void main(String[] args) {
		connect(args);

		byte[] imgData = imageToByteArray(imagePath);
		Logger.getLogger(Main.class.getName()).log(Level.INFO, "Sending " + imgData.length + " bytes to server...");
		ECHO.sendBytes(imgData);

		Logger.getLogger(Main.class.getName()).log(Level.INFO, "DONE!");
	}

	private static void connect(String[] args) {
		try {
			// create and initialize the ORB
			ORB orb = ORB.init(args, null);

			String stringIOR;
			try (BufferedReader fileReader = new BufferedReader(new FileReader(SERVER_IOR_FNAME))) {
				stringIOR = fileReader.readLine();
			}

			// get the root naming context
			org.omg.CORBA.Object objRef = orb.string_to_object(stringIOR);
			ECHO = EchoHelper.narrow(objRef);
		} catch (Exception e) {
			System.out.println("ERROR : " + e);
			e.printStackTrace(System.out);
		}
	}

	private static byte[] imageToByteArray(String fname) {
		File file = new File(fname);

		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		try {
			for (int readNum; (readNum = fis.read(buf)) != -1;) {
				// Writes to this byte array output stream
				bos.write(buf, 0, readNum);
			}
		} catch (IOException ex) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
		}

		return bos.toByteArray();

	}
}
